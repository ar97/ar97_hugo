---
title: "Re_stacks"
date: 2020-07-17T11:02:43+05:30
draft: false
toc: false
images:
tags:
  - Bon Iver
  - Lyrics
  - Songs
  - Re: Stacks 
---

# Lyrics to the song Re: Stacks
## Bon Iver

This my excavation and to
Day is Qumran
Everything that happens is from now on
This is pouring rain
This is paralyzed

I keep throwing it down, two
Hundred at a time
It's hard to find it when you knew it
When your money's gone
And you're drunk as hell

On your back with your racks as he stacks your load
In the back with the racks and he stacks your load
In the back with the racks and you're unstacking your load
I've been twisting to the sun
I needed to replace
And the fountain in the front yard is rusted out
All my love was down
In a frozen ground

There's a black crow sitting across from me
His wiry legs are crossed
He's dangling my keys, he even fakes a toss
Whatever could it be
That has brought me to this… 
