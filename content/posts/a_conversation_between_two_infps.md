---
title: "A conversation between two INFPs"
date: 2020-07-06T15:36:19+05:30
draft: false 
toc: false
images:
tags:
  - conversations 
  - rants
  - nonesense
---


**INFP#1**:  Did you talk to her about this ?

_**INFP#2**_:  Yeah, I did actually. We had a good laugh about it.

**INFP#1**:  You're lucky. I'm glad that you have someone like that.

_**INFP#2**_:  I mean yeah, we were friends before we became something, you know? We have that cushion to fall back into or whatever. And yeah I'm glad too; to have her by my side. I don't know. Do success rates of relationships depend greatly on good communication. From time to time, it feels like it's getting more and more difficult to put labels on feelings and thoughts. Like its getting harder to be precise when I'm trying to say something.

**INFP#1**:  It's good to be precise when you're communicating but why do you have to put labels on everything? Is that really necessary ? Won't the listener have some idea about what you're trying to say ?

_**INFP#2**_: Yeah but, as we go along, when we start spending time with someone, a significant amount of time that is. I get the feeling that we develop a way of identifying with things, emotions, and words other person is uttering. This is not based on some literature or something. Just consider this a thought experiment. According to our brain, isn't every word a label?

**INFP#1**:  ......

_**INFP#2**_:  Let me ask you this. Aren't words abstractions? Or - wouldn't it be fair to say that many words have a certain degree of abstraction to iti? Especially the words we use to communicate our feelings? Words are just sounds that we produce to tell each other about How we feel? about things? to let the other person know that "Hey there's a Tiger coming into our cave, you might wanna run". I mean we use language to tell each other just about everything. And I like to think that these languages are more abstract than we think they are.

**INFP#1**:  ......

_**INFP#2**_:  Hello?

**INFP#1**:  Yeah! Sorry, little Jeni started crying. I had to put her back to sleep. Yeah I agree. I have felt that too. Words can have variations in the way it is understood from person to person. I get that.

_**INFP#2**_:  Yeah, Alright. So the question is, how can we be sure that the person listening, is able to comprehend what we're saying the way we mean it?

**INFP#1**:  We can never be absolutely sure. But we can be confident to a certain degree; if we try really, really hard.

_**INFP#2**_:  I think we can only take into consideration, the probability that the person listening might misunderstand us. There will always be a certain level of ambiguity in what we speak.

**INFP#1**:  Alright, So where are you getting at ?

_**INFP#2**_:  Nowhere. I was saying that we humans are barely communicating. It's like saying "Oh that tyre is round just enough so that we can keep it spinning". Being effective at that? It is a whole different story. Words are just incapable of capturing all those colors and feelings associated to what we are trying to say. Maybe I ought to read more. Maybe my vocabulary is insufficient maybe that's why I'm feeling like this. And hey, don't take this the wrong way, there are people who are really, really good at communication, but what I'm trying to say is, that's not the case for most of us.

**INFP#1**:  Right.

_**INFP#2**_:  We're just getting by using generalization and approximations and we are too damn good at it. I am sick of these rounding errors. Tell me. Tell me now, How can I be sure that when I'm talking about a chicken steak burger, you won't be thinking about a sandwich ?

**INFP#1**:  Uhh.. You can't. Or at least not entirely.

_**INFP#2**_:  I think I'm going crazy.

**INFP#1**:  That's another thing. I wonder who defines crazyness. When you think about it, when a certain voltage is reached, a diode has a breakdown and it starts working. How would you measure the breaking point of a human mind?

_**INFP#2**_:  ....

**INFP#1**:  Would you call the raw version of yourself crazy? Why I'm asking you is because I feel like the way you behave sometimes or the how you think about things does not really fall under the generally accepted structures of normality.

_**INFP#2**_:  I am not sure of what the socially accepted standard of craziness is but, being crazy can mean a thousand different things right?

**INFP#1**:  JUST ANSER THE QUESTION. WOULD YOU OR WOULD YOU NOT CALL THE 'RAW' VERSION OF YOURSELF CRAZY?

_**INFP#2**_:  Uhh.., I wouldn't call myself crazy but when we're talking about things that goes on inside my head, yeah. Sometimes they tend to deviate from what is normal. At least that's what I think. But maybe everyone feels like that, maybe my understanding of normality is impaired. These are all just possibilities, you know? Maybe I'm a serial killer; like John Doe from Se7en. I just don't know it yet. I hope not. 

**INFP#1**:  Hmm.  

_**INFP#2**_:  But the sad part is, what if all of this does not make any sense to the person who's reading all this? Just like I said, what if the ideas that I am trying to put across is being lost in between all of these really ambigous words?

**INFP#1**:  I'm sorry but you can't do anything about it. But let's hope that it will make sense to them.

_**INFP#2**_:  I guess.. But in a way that's the beauty of it too. One sentance can have a thousand different meanings when a thousand different people  read it. If everyone felt the same way, knew the same things, when they hear or see a word, we wouldn't even be trying to explain things to each other anymore. People will just use single word sentances for everything, and that word will be all inclusive of everything they are trying to communicate. That's not good, right? Can you imagine living like that? It's very 'One dimensional'. I guess things are better this way. We can just live our lives; explaining things to each other. We can tell each other stories and jokes and people can relate to them in their own unique way. I wouldn't want my story to end in two words, that wouldn't even be qualified to be a story. 

**INFP#1**:  I think you're thinking too much .. ^_^

_**INFP#2**_: I guess you're right. ᕕ( ՞ ᗜ ՞ )ᕗ 

**INFP#1**:  These emojies are cool ^^

_**INFP#2**_:  If you want more you can search for them. They're called 'Kaomojies', If you're wondering what that is, It's a combination of two words in Kanji, 'Kao' which means face, and 'Moji' which means character. 

**INFP#1**: Hmm... I feel tired.

_**INFP#2**_: I know.

