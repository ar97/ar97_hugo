---
title: "Why FOSS"
date: 2020-07-06T17:37:32+05:30
draft: false 
---


## That is the question. Isn't it ?

- Why would you let someone choose the words you speak?
- What if you were forced to speak with a certain set of words?
- Why would you give up the freedom to learn, share and improve what you have?
- Why would you give up the control over your own "things"?
